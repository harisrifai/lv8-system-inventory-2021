<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Supplies;
use App\Models\Search;
use Carbon\Carbon;
 
class Service extends Component
{
    public $query;
    public $active; 
    public $search = '';
    public   $searchTerm; 
    public $sortBy = 'type';
    public $sortAsc = true;
    public $item;
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function render()
    {
         
       // $searchs = Supplies::where('current_team_id', auth()->user()->current_team_id)->where('active', 1)->get();
     
        //return view('livewire.service',compact('searchs'));
        sleep(1);  
        $query='%'.$this->query.'%'; 
        $searchs = Supplies::SELECT('id','sku','barcode','type','name','detail','vendor','received','return','adjust','qty','min_qty','price','tot_price','remark','buy_date','order_date' ,'current_team_id','active')->where('current_team_id', auth()->user()->current_team_id)
            ->whereYear('created_at', '=', Carbon::now()->year)
            ->where('sku', 'like', $query)
            ->orwhere('barcode', 'like', $query)
            ->orwhere('name', 'like', $query)
            ->get();
             
        return view('livewire.transaction.service', ['searchs' => $searchs,
        ]);


    }
     
}
